import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  authors:any = [{id:1 ,name:'Lewis Carrol' }, {id:2 , name: 'Leo Tolstoy'}, {id:3 ,name:'Thomas Mann'}];
  i:number =3;
  
  getAuthors(): any {
    const authorsObservable = new Observable(
      observer => {
           setInterval(() =>  observer.next(this.authors)
           , 4000)
    })
    return authorsObservable;
}

addAuthors(newAuthor:string){
  this.i=this.i+1;
  // setInterval(() =>  this.authors.push({id:this.i, name: newAuthor})); 
    this.authors.push({id:this.i, name: newAuthor}); 
}

  
  getBooks():Observable<any>{
   return this.db.collection('books').valueChanges();
  }
  constructor(private db:AngularFirestore) { }
}
