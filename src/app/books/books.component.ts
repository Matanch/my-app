import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorsService } from '../authors.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  //books:object[]=[{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'TOY STORY', author:'Matan Chover'},]
  books:any;
  books$:Observable<any>;
  constructor(private booksservice:AuthorsService) { }

  ngOnInit() {
    this.books$ = this.booksservice.getBooks();
  }

}
