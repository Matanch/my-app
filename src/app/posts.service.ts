import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BlogPosts} from './interfaces/blog-posts';
import {Users} from './interfaces/users';
import { AngularFirestore,AngularFirestoreModule, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private APIPosts = "https://jsonplaceholder.typicode.com/posts/";
  private APIUsers = "https://jsonplaceholder.typicode.com/users/";

    constructor(private http:HttpClient,private db:AngularFirestore)  { }

    
  getBlogPost():Observable<BlogPosts[]>{
  return this.http.get<BlogPosts[]>(`${this.APIPosts}`);
   }
   getUserPost(): Observable<Users[]>{
    return this.http.get<Users[]>(`${this.APIUsers}`);
   }
   addPost(body:String,author:String,title:String){
    const posts = {body:body, author:author,title:title}
    this.db.collection('posts').add(posts);
  }

}



