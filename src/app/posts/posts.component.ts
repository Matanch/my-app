import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import {Users} from './../interfaces/users';
import {BlogPosts} from './../interfaces/blog-posts';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
  posts$:BlogPosts[]=[];
  users$;
  title:string; 
  body:string;
  author:string;
  message:String;


  constructor(private postsrvice:PostsService) { }

  saveFunc(){
    for (let index = 0; index < this.posts$.length; index++) {
      for (let i = 0; i < this.users$.length; i++) {
        if (this.posts$[index].userId==this.users$[i].id) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.author = this.users$[i].name;
          this.postsrvice.addPost(this.body, this.author,this.title);
          
        }
        
        
      }
      
    }
    this.message ="The data loading was successful"
  }
  
  ngOnInit() {
   this.postsrvice.getBlogPost().subscribe(data =>this.posts$ = data);
   this.postsrvice.getUserPost().subscribe(data =>this.users$ = data);
      //this.users$ = this.postsrvice.getUserPost();

  }

}
