// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
  apiKey: "AIzaSyCyvAVFszXjYGfGLQXMoRYXo279vIWtHS8",
  authDomain: "homework-cc790.firebaseapp.com",
  databaseURL: "https://homework-cc790.firebaseio.com",
  projectId: "homework-cc790",
  storageBucket: "homework-cc790.appspot.com",
  messagingSenderId: "900551809919",
  appId: "1:900551809919:web:720942dd7cec5e8220184a",
  measurementId: "G-QF5852JSZ1"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
